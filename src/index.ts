let primes = getInitPrimes()

function getInitPrimes() {
    return [
        2,
        3
    ]
}

function getLastPrime(){
    return primes[primes.length - 1]
}

let log = (...str:any)=>{}

/**
 * @name calculatePrimes
 * 
 * @param until max number to calculate to | NOTICE: DOES NOT MEAN THAT lastPrime or the latest prime IS UNDER THAT NUMBER
 */
function calculatePrimes(until:number){
    let currentNumber=getLastPrime()
    function increment(){
        currentNumber=currentNumber+2
        log("Called Increment", currentNumber)
        let isPrime = true
        for (let index = 0; index < primes.length; index++) {
            log("For Loop", currentNumber)
            const element = primes[index];
            if (currentNumber%element==0){
                isPrime=false
                break
            }
        }
        if (isPrime){
            primes.push(currentNumber)
        }
        // primes.forEach(element => {
        //     if (currentNumber%element==0){
        //         isPrime=false
        //     }
        // });
        if (until <= currentNumber) {
            return {
                "output": {
                    "lastPrime": getLastPrime(),
                    "primes": primes
                },
                "sendOutput":true
            }
        } else {
            return {
                "sendOutput":false
            }
        }
    }
    while (true) {
        let o = increment()
        if (o.sendOutput==true){
            return o.output
        }
    }
}

function resetPrimes(){
    primes=getInitPrimes()
}

module.exports = {
    getLastPrime: getLastPrime,
    calculatePrimes: calculatePrimes,
    resetPrimes: resetPrimes
}
